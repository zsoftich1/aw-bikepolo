# Bike Polo

Command line utility showing how suitable current weather conditions are for bike polo.

## Development

### Setup
Setup a virtualenv and install requirements

```make init```

Add your AerisWeather client_id and client_secret to your environment variables.

```
export AERISWEATHER_CLIENT_ID=<client_id>
export AERISWEATHER_CLIENT_SECRET=<client_secret>
```

The project uses default python `black` formatting.

To run using input data rather than making an API request run `make run` or supply the `--file <jsonsource>` option with a previously retrieved json response.

### Testing
To run `pytest`'s

```make test```

## Future

### TODO
- Add air quality index (AQI) to calculation
- Use a weighted calculation for precipitation in relation to temperature
- How is health and age a factor in rating?


### AW API Questions
- Is rate of precipitation (snowRateIN, precipRateIN) appropriate for forecasted data or is it only current?
- Include probability of precipitation in calculation
    - How does this relate to rate of precipitation?
