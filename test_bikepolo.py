import pytest

from click import BadOptionUsage
from click.testing import CliRunner

import bikepolo
from bikepolo_index import BikePoloIndex, Index


runner = CliRunner()


def test_is_valid_location():
    def ivl(loc):
        return bikepolo.is_valid_location(None, "--location", loc)

    loc = ":auto"
    assert ivl(loc) == loc
    loc = "55403"
    assert ivl(loc) == loc
    loc = "5540"
    with pytest.raises(BadOptionUsage):
        ivl(loc)
    loc = "minneapolis,mn"
    assert ivl(loc) == loc
    loc = "M8nneapolis,mn"
    with pytest.raises(BadOptionUsage):
        ivl(loc)
    loc = "45.0139,-93.1545"
    assert ivl(loc) == loc
    loc = "91.0139,-93.1545"
    with pytest.raises(BadOptionUsage):
        ivl(loc)
    loc = "45.0139,-181.1545"
    with pytest.raises(BadOptionUsage):
        ivl(loc)
    loc = "MSP"
    assert ivl(loc) == loc
    loc = "msp"
    assert ivl(loc) == loc
    loc = "mspa"
    with pytest.raises(BadOptionUsage):
        ivl(loc)


def test_is_valid_datetime():
    def ivd(dt):
        return bikepolo.is_valid_datetime(None, "--from", dt)

    valid_dts = [
        "tomorrow",
        "+1hr",
        "1302883980",
        "friday",
        "2022/10/09",
        "2017-02-27 5:00 PM",
    ]
    for dt in valid_dts:
        assert ivd(dt) == dt
    invalid_dts = ["foo", "someday", "1302883"]
    for dt in invalid_dts:
        with pytest.raises(BadOptionUsage):
            ivd(dt)


def test_bikepolo_index():
    def test(update, result):
        data = {
            "dateTimeISO": "2022-10-07T14:32:00-05:00",
            "feelslikeF": 37.41,
            "precipRateIN": 0,
            "snowRateIN": 0,
        }
        data.update(update)
        bpi = BikePoloIndex(**data)
        assert bpi.index == result

    test({"precipRateIN": 0.31}, Index.BAD)
    test({"snowRateIN": 0.04}, Index.BAD)
    test({"feelslikeF": 27.41}, Index.BAD)
    test({}, Index.POOR)
    test({"feelslikeF": 67.41, "precipRateIN": 0.2}, Index.POOR)
    test({"feelslikeF": 47.41}, Index.GOOD)
    test({"feelslikeF": 47.41}, Index.GOOD)
    test({"feelslikeF": 65.41, "precipRateIN": 0.09}, Index.VERYGOOD)
    test({"feelslikeF": 77.41}, Index.EXCELLENT)
    test({"feelslikeF": 77.41, "precipRateIN": 0.09}, Index.EXCELLENT)
