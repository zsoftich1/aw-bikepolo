init:
	python -m venv .venv
	python -m pip install -r requirements.txt

run:
	python bikepolo.py --file test_data.json

format:
	black bikepolo.py

test:
	python -m py.test test_bikepolo.py