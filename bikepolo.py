import click
import dateparser
import json
import os
import re
import requests
import sys

from pprint import pprint

from bikepolo_index import BikePoloIndex, BikePoloIndexDisplay


client_id = os.environ.get("AERISWEATHER_CLIENT_ID")
client_secret = os.environ.get("AERISWEATHER_CLIENT_SECRET")

if client_id is None or client_secret is None:
    click.echo(
        "Missing AerisWeather client credentials. Please ensure AERISWEATHER_CLIENT_ID and AERISWEATHER_CLIENT_SECRET are set in your environment"
    )
    sys.exit(1)

AUTH_PARAMS = {"client_id": client_id, "client_secret": client_secret}
ENDPOINT_AE_API = "https://api.aerisapi.com"
ENDPOINT_AE_API_CONDITIONS = f"{ENDPOINT_AE_API}/conditions"
WEATHER_FIELDS = [
    "periods.dateTimeISO",
    "periods.feelslikeF",
    "periods.weatherPrimaryCoded",
    "periods.uvi",
    "periods.precipIN",
    "periods.precipRateIN",
    "periods.snowIN",
    "periods.snowRateIN",
    "periods.pop",
    "loc.lat",
    "loc.long",
    "place.country",
    "place.name",
    "place.state",
]

# Location regex
REGEX_COORD = re.compile(
    r"^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$"
)
REGEX_CITY_STATE_COUNTRY = re.compile(r"^[A-Za-z]+,[A-Za-z]+(,[A-Za-z]+)?")
REGEX_ZIP = re.compile(r"(^\d{5}$)|(^\d{9}$)|(^\d{5}-\d{4}$)")
REGEX_AIRPORT_CODE = re.compile(r"^[A-Za-z]{3}")


def is_valid_location(ctx, opt, value):
    """Tests if location is valid and returns location value else raises click.BadOptionUsage.

    :param ctx: click context
    :param opt: name of click option
    :returns: value
    :raises click.BadOptionUsage: raised when location does not pass validation
    """
    if (
        REGEX_ZIP.fullmatch(value)
        or REGEX_CITY_STATE_COUNTRY.fullmatch(value)
        or REGEX_COORD.fullmatch(value)
        or REGEX_AIRPORT_CODE.fullmatch(value)
        or value == ":auto"
    ):
        return value
    raise click.BadOptionUsage(
        opt, message="format must be a zipcode, 'lat,long' or 'city,state'"
    )


def is_valid_datetime(ctx, opt, value):
    """Tests if datetime is valid.

    This may not be accurate as it uses the dateparser module to test PHP strtotime() functionality rather than strictly following PHP strtotime().

    :param ctx: click context
    :param opt: name of click option
    :param value: value to be tested
    :returns: value
    :raises click.BadOptionUsage: raised when datetime string cannot be converted
    """
    error = False
    try:
        dt = dateparser.parse(value)
    except (ValueError, TypeError):
        error = True
    if dt is None or error:
        raise click.BadOptionUsage(opt, message="Not a recognized datetime format")
    return value


def get_weather_conditions(loc, interval, from_, to):
    uri = f"{ENDPOINT_AE_API_CONDITIONS}/{loc}"
    params = {}
    params.update({"from": from_, "to": to, "filter": interval, "plimit": 1})
    params.update({"fields": ",".join(WEATHER_FIELDS)})
    params.update(AUTH_PARAMS)
    return requests.get(uri, params=params)


@click.command()
@click.option(
    "-l",
    "--location",
    default=":auto",
    callback=is_valid_location,
    help="location to check. Format must be zipcode, 'lat,long' or 'city,state'. defaults to autodetection from IP geolocation.",
)
@click.option(
    "-f",
    "--from",
    "from_",
    default="now",
    callback=is_valid_datetime,
    help="Valid datetime such as '2022-10-09 5:00PM'. Defaults to now.",
)
@click.option("-t", "--to", default="+1hours", hidden=True, callback=is_valid_datetime)
@click.option("-i", "--interval", default="1hr", hidden=True)
@click.option("--file", type=click.File("r"), hidden=True)
def main(location, from_, to, interval, file):
    """BikePolo? Retrieves weather data and rates the suitability of bikepolo from 1(bad) to 5(Excellent) for a one hour period."""
    if file:
        with file:
            data = json.loads(file.read())

    else:
        try:
            resp = get_weather_conditions(location, interval, from_, to)
            resp.raise_for_status()
        except requests.exceptions.HTTPError as err:
            SystemExit(err)
        except requests.exceptions.RequestException as err:
            SystemExit(err)
        data = resp.json()

    period = data["response"][0]["periods"][0]
    location = data["response"][0]["loc"]
    place = data["response"][0]["place"]
    bpi = BikePoloIndex(location=location, place=place, **period)
    bpid = BikePoloIndexDisplay()
    bpid.cli(bpi)
    sys.exit(0)


if __name__ == "__main__":
    main()
