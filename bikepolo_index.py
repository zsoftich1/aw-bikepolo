from collections import namedtuple
from enum import Enum, unique
import textwrap

MAX_TEMP = 999
MIN_TEMP = -999
MIN_PRECIP = 0.0001
MAX_PRECIP = 999


@unique
class IndexNamed(Enum):
    UNKNOWN = "Unknown"
    BAD = "Bad"
    POOR = "Poor"
    GOOD = "Good"
    VERYGOOD = "Very Good"
    EXCELLENT = "Excellent"


@unique
class Index(Enum):
    UNKNOWN = 0
    BAD = 1
    POOR = 2
    GOOD = 3
    VERYGOOD = 4
    EXCELLENT = 5

    def __str__(self):
        return f"{self.value} - {IndexNamed[self.name].value}"


TemperatureRangeIndex = namedtuple("TemperatureRangeIndex", "low high index")

BikePoloTemperatureIndicesF = (
    TemperatureRangeIndex(MIN_TEMP, 32, Index.BAD),
    TemperatureRangeIndex(32, 40, Index.POOR),
    TemperatureRangeIndex(40, 65, Index.GOOD),
    TemperatureRangeIndex(65, 70, Index.VERYGOOD),
    TemperatureRangeIndex(70, 80, Index.EXCELLENT),
    TemperatureRangeIndex(80, 85, Index.VERYGOOD),
    TemperatureRangeIndex(85, 90, Index.GOOD),
    TemperatureRangeIndex(90, 100, Index.POOR),
    TemperatureRangeIndex(100, MAX_TEMP, Index.BAD),
)


@unique
class RainIntensity(Enum):
    UNKNOWN = 0
    LIGHT = 1
    MODERATE = 2
    HEAVY = 3
    VIOLENT = 4


# Taken from https://www.baranidesign.com/faq-articles/2020/1/19/practical-guide-to-determining-rainfall-rate-and-rain-intensity-error
RainIntensityINPerHr = namedtuple("RainIntensityINPerHr", "low high index")
RainIntensitiesINPerHr = (
    RainIntensityINPerHr(MIN_PRECIP, 0.1, RainIntensity.LIGHT),
    RainIntensityINPerHr(0.1, 0.3, RainIntensity.MODERATE),
    RainIntensityINPerHr(0.3, 2.0, RainIntensity.HEAVY),
    RainIntensityINPerHr(2.0, MAX_PRECIP, RainIntensity.VIOLENT),
)


@unique
class SnowIntensity(Enum):
    UNKNOWN = 0
    LIGHT = 1
    MODERATE = 2
    HEAVY = 3


# Taken from https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiUyJSYxtP6AhXVIH0KHS2mAR4QFnoECAwQAQ&url=http%3A%2F%2Fwww.icao.int%2Fsafety%2Fmeteorology%2Famofsg%2FAMOFSG%2520Meeting%2520Material%2FAMOFSG.7.IP.004.5.en.doc&usg=AOvVaw1jlhRR9cWfXaSlFoC64pNP
SnowIntensityINPerHr = namedtuple("SnowIntensityINPerHr", "low high index")
SnowIntensitiesINPerHr = (
    SnowIntensityINPerHr(MIN_PRECIP, 0.039, SnowIntensity.LIGHT),
    SnowIntensityINPerHr(0.039, 0.098, SnowIntensity.MODERATE),
    SnowIntensityINPerHr(0.098, MAX_PRECIP, SnowIntensity.HEAVY),
)


def get_precipitation_intensity(intensities, precipitation, unknown):
    for intensity in intensities:
        if precipitation >= intensity.low and precipitation <= intensity.high:
            return intensity.index
    return unknown


def get_bikepolo_temerature_index(temperature):
    for tri in BikePoloTemperatureIndicesF:
        if temperature >= tri.low and temperature <= tri.high:
            return tri.index
    return Index.UNKNOWN


class BikePoloIndex(object):
    def __init__(
        self,
        feelslikeF,
        precipRateIN,
        snowRateIN,
        dateTimeISO,
        location=None,
        place=None,
        **kwargs,
    ):
        self.feelslikeF = feelslikeF
        self.precipRateIN = precipRateIN
        self.snowRateIN = snowRateIN
        self.dateTimeISO = dateTimeISO
        self.location = location
        self.place = place
        self.__dict__.update(kwargs)
        self._calculate_index()

    def __repr__(self):
        return f"<{self.__class__.__name__} index={self.index.value}>"

    def _calculate_index(self):
        temperature_index = get_bikepolo_temerature_index(self.feelslikeF)
        rain_intensity = get_precipitation_intensity(
            RainIntensitiesINPerHr, self.precipRateIN, RainIntensity.UNKNOWN
        )
        snow_intensity = get_precipitation_intensity(
            SnowIntensitiesINPerHr, self.snowRateIN, SnowIntensity.UNKNOWN
        )

        index = temperature_index
        if rain_intensity in (
            RainIntensity.VIOLENT,
            RainIntensity.HEAVY,
        ) or snow_intensity in (
            SnowIntensity.HEAVY,
            SnowIntensity.MODERATE,
        ):
            index = Index.BAD
        elif rain_intensity is RainIntensity.MODERATE and temperature_index in (
            Index.EXCELLENT,
            Index.VERYGOOD,
        ):
            index = Index.POOR
        elif rain_intensity is RainIntensity.MODERATE and temperature_index in (
            Index.GOOD,
            Index.POOR,
            Index.BAD,
        ):
            index = Index.BAD
        elif rain_intensity is RainIntensity.LIGHT and temperature_index in (
            Index.EXCELLENT,
            Index.VERYGOOD,
        ):
            index = temperature_index

        self.index = index


class BikePoloIndexDisplay(object):
    def __init__(self):
        self._cli_format = textwrap.dedent(
            """
            Location: {} ({})
            Time: {}
            Activity: BikePolo
            Rating Index: {}
            """
        )

    def cli(self, bpi):
        """Output a BikePoloIndex to the console.

        :param bpi: a BikePoloIndex object
        """
        location = "N/A"
        coords = "N/A"
        if bpi.place:
            location = ",".join(
                (
                    bpi.place.get("name"),
                    bpi.place.get("state"),
                    bpi.place.get("country"),
                )
            )
        if bpi.location:
            coords = ",".join(
                (str(bpi.location.get("lat")), str(bpi.location.get("long")))
            )
        print(self._cli_format.format(location, coords, bpi.dateTimeISO, bpi.index))
